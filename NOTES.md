# Notes

- https://michaelnthiessen.com/options-object-pattern
- https://vuejs.org/guide/reusability/composables#conventions-and-best-practices
- https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
- https://developer.mozilla.org/en-US/docs/Web/API/Document
- https://developer.mozilla.org/en-US/docs/Web/API/EventTarget
- https://vueuse.org/core/useEventListener/
- https://vuejs.org/guide/reusability/composables#mouse-tracker-example: `onMounted(() => window.addEventListener('mousemove', update))`
- [oxlint](https://oxc-project.github.io/docs/guide/usage/linter.html):
  - https://github.com/oxc-project/oxc/blob/oxlint_v0.2.8/crates/oxc_linter/src/rules.rs
  - https://github.com/oxc-project/oxc/tree/oxlint_v0.2.8/crates/oxc_linter/src/rules
  - https://github.com/oxc-project/oxc/blob/oxlint_v0.2.8/crates/oxc_linter/src/rules/eslint/eqeqeq.rs
  - https://github.com/oxc-project/oxc/issues/732
  - https://github.com/oxc-project/oxc/issues/736
  - https://github.com/oxc-project/oxc/issues/2093
  - https://github.com/oxc-project/oxc/issues/2023
  - https://github.com/oxc-project/oxc/pull/2130
  - https://github.com/oxc-project/oxc/blob/oxlint_v0.2.8/crates/oxc_linter/src/javascript_globals.rs
  - https://github.com/oxc-project/oxc/pull/2130/files#diff-1e372f17620d76aeca40fd67714affabb0f26064504b8efde5eced8d36cd42e2
  - https://github.com/oxc-project/oxc/tree/oxlint_v0.2.8/crates/oxc_cli/fixtures/eslintrc_env
  - https://github.com/oxc-project/oxc/blob/oxlint_v0.2.8/crates/oxc_cli/fixtures/eslintrc_env/eslintrc_env_browser.json
  - https://eslint.org/docs/latest/use/configure/language-options#specifying-environments
  - https://eslint.org/docs/latest/use/configure/language-options#using-configuration-files
  - https://stackoverflow.com/questions/76335327/eslint-does-not-recognise-documenteventmap-complaining-no-undef
  - https://typescript-eslint.io/linting/troubleshooting/#i-get-errors-from-the-no-undef-rule-about-global-variables-not-being-defined-even-though-there-are-no-typescript-errors
  - https://eslint.org/docs/latest/use/configure/language-options#specifying-globals
  - https://github.com/typescript-eslint/typescript-eslint/blob/v7.0.1/packages/scope-manager/src/lib/dom.ts#L506
  - https://github.com/typescript-eslint/typescript-eslint/blob/v7.0.1/packages/scope-manager/src/lib/base-config.ts
- https://patterns.dev/vue/composables/
- https://github.com/pikax/vue-composable/blob/879528a1aa7633b9c594021d196ab9df37c41ddd/packages/vue-composable/src/event/event.ts
- https://github.com/nruffing/native-event-vue/blob/24166ea356c415d78acef9daf501a8e2ef592f72/lib/composables/useNativeEvent.ts
- https://logaretm.com/blog/my-favorite-5-vuejs-composables/
- https://antfu.me/posts/composable-vue-vueday-2021#side-effects-self-cleanup
- https://developer.mozilla.org/en-US/docs/Web/API/Element/click_event
- https://github.com/vueuse/vueuse/blob/v10.7.2/packages/shared/utils/types.ts#L6: `export type Fn = () => void`
- https://github.com/vueuse/vueuse/blob/v10.7.2/packages/core/useEventListener/index.ts#L65
- https://qwik.dev/api/qwik/#useondocument
- https://github.com/BuilderIO/qwik/blob/v1.4.4/packages/qwik/src/core/use/use-on.ts#L63
- https://www.typescriptlang.org/docs/handbook/2/functions.html#function-overloads
- https://github.com/microsoft/TypeScript/blob/v5.3.3/src/lib/dom.generated.d.ts#L6713
- https://github.com/microsoft/TypeScript/blob/v5.3.3/src/lib/dom.generated.d.ts#L7351
- https://github.com/sindresorhus/globals

## Commands

```bash
npm install vue && npm install -D vite @vitejs/plugin-vue typescript vue-tsc create-vite-tsconfigs sort-package-json npm-run-all2 prettier oxlint
```

```bash
rm -rf node_modules/ && npm install
```

```bash
npx oxlint --rules
```

```bash
npx oxlint --help
```

```bash
npx oxlint -D all --import-plugin
```
