import { onMounted, onBeforeUnmount } from "vue";

export function useEventOnDocument<E extends keyof DocumentEventMap>(
  event: E,
  handler: (this: Document, ev: DocumentEventMap[E]) => any,
  options?: boolean | AddEventListenerOptions,
): void {
  onMounted(() => {
    document.addEventListener(event, handler, options);
  });

  onBeforeUnmount(() => {
    document.removeEventListener(event, handler, options);
  });
}
