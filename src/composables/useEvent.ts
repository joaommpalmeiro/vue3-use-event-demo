import { onMounted, onBeforeUnmount } from "vue";

export function useEvent(
  event: string,
  handler: EventListener,
  options?: { target: EventTarget } & AddEventListenerOptions,
): void {
  const { target = document, ...listenerOptions } = options ?? {};

  onMounted(() => {
    target.addEventListener(event, handler, listenerOptions);
  });

  onBeforeUnmount(() => {
    target.removeEventListener(event, handler, listenerOptions);
  });
}
